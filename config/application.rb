require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Cins
  class Application < Rails::Application

    config.time_zone = 'Tokyo'

    # twitter api
    config.tw_consumer_key = ENV["TW_CONSUMER_KEY"]
    config.tw_consumer_secret = ENV["TW_CONSUMER_SECRET"]
    config.tw_access_token = ENV["TW_ACCESS_TOKEN"]
    config.tw_access_token_secret = ENV["TW_ACCESS_TOKEN_SECRET"]

  end
end
