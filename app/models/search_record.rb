class SearchRecord < ApplicationRecord

  class << self

    def start_of_campaign
      Rails.cache.fetch 'SearchRecord.self/start_of_campaign' do
        order(until: :asc).take(1).first.since
      end
    end

    def latest_update
      Rails.cache.fetch 'SearchRecord.self/latest_update' do
        _latest_update.first
      end
    end

    def latest_update_timestamp
      return nil if latest_update.nil?
      latest_update.until
    end

    def needs_update(current = Time.current)
      _update_targets(current).exists?
    end

    def needs_user_caching
      _user_caching_targets.exists?
    end

    def update(current = Time.current)
      create_tweet_meta_data
      _update_targets(current).each do |record|
        record.search
        record.create_tweet_meta_data
      end

      timestamp = _latest_update.first
      Idol.all.each{ |idol| idol.create_caches timestamp }
      Rails.cache.write 'SearchRecord.self/latest_update', timestamp
    end

    def create_tweet_meta_data
      _user_caching_targets.each(&:create_tweet_meta_data)
    end

    private

    def _latest_update
      where(done: true).order(until: :desc).take(1)
    end

    def _update_targets(current)
      where(done: false).where(arel_table[:until].lt(current - 1.hours))
    end

    def _user_caching_targets
      where(done: true).where(cached: false)
    end

  end

  def search_query
    '【アイドルマスターシンデレラガールズ】 総選挙開催中!! #シンデレラガール総選挙'
  end

  def idol_regex
    /![\s\n]*([^\s]+)に投票したよ/
  end

  def retweet_regex
    /^RT @.*/
  end

  def target_source
    '<a href="http://sp.pf.mbga.jp/12008305" rel="nofollow">アイドルマスター シンデレラガールズ公式</a>'
  end

  def default_client
    Twitter::REST::Client.new do |config|
      config.consumer_key        = Cins::Application.config.tw_consumer_key
      config.consumer_secret     = Cins::Application.config.tw_consumer_secret
      config.access_token        = Cins::Application.config.tw_access_token
      config.access_token_secret = Cins::Application.config.tw_access_token_secret
    end
  end

  def search(client: default_client, query: search_query)
    idols = Idol.all

    Tweet.transaction do
      tweets = _search_twitter(client, query).map do |t|
        Tweet.find_or_create_by! id: t.id do |tweet|
          tweet.idol = _find_idol(idols, t.text)
          tweet.json = t.attrs.to_json
          tweet.created_at = t.created_at
        end
        t.id
      end

      next_id = tweets.min.to_s
      update! max_id: next_id

      latest_id = _search_twitter(client, query).take(2).map(&:id).min.to_s
      update! done: latest_id.nil? || next_id.eql?(latest_id)

      tweets.count - 1
    end
  end

  def _find_idol(idols, query)
    return nil if query.match(retweet_regex)

    regex_match = query.match(idol_regex)

    unless regex_match.nil?
      name = regex_match[1]
      idol = Idol.find_idol(idols, name)
      return idol unless idol.nil?
    end

    Idol.find_idol(idols, query)
  end

  def create_tweet_meta_data
    tweet_timestamp = Tweet.arel_table[:created_at]
    tweets = Tweet.includes(:user).where(tweet_timestamp.gteq(since)).where(tweet_timestamp.lteq(self.until))

    Tweet.transaction do
      User.transaction do
        source = target_source
        tweets.each do |tweet|
          json = tweet.json_hash
          user_json = json['user']
          if tweet.user.nil?
            tweet.user = User.find_or_create_by! id: user_json['id'] do |u|
              u.screen_name = user_json['screen_name']
              u.user_name = user_json['name']
              u.profile_image_url = user_json['profile_image_url']
            end
          end
          tweet.source_checked = source.eql? json['source'].to_s
          tweet.save!
        end
        self.cached = true
        save!
      end
    end
  end

  private

  def _search_twitter(client, query)
    client.search(
      query,
      result_type: :recent,
      max_id: max_id,
      since: since.to_s(:twitter_search),
      until: self.until.to_s(:twitter_search),
    )
  end

end
