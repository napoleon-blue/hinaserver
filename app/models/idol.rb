class Idol < ApplicationRecord
  has_many :tweets
  has_many :users, through: :tweets
  validates :name, presence: true
  validates :yomi, presence: true

  class << self

    def all_idols
      key = "Idol.self/all_idols/#{SearchRecord.latest_update_timestamp}"
      Rails.cache.fetch key do
        all.to_a
      end
    end

    def tweet_count_map
      key = "Idol.self/tweet_count_map/#{SearchRecord.latest_update_timestamp}"
      Rails.cache.fetch key do
        Tweet.valid_tweets
          .group(:idol_id).count.to_h
      end
    end

    def user_count_map
      key = "Idol.self/user_count_map/#{SearchRecord.latest_update_timestamp}"
      Rails.cache.fetch key do
        Tweet.valid_tweets
          .select(:idol_id, :user_id)
          .group(:idol_id)
          .count('distinct user_id').to_h
      end
    end

    def find_idol(idols, query)

      return nil if query.nil?

      matched = idols.find { |idol| query.include? idol.display_name }
      return matched unless matched.nil?

      partial_matched = idols.find { |idol|
        parts = idol.name.split(/[,・]/)
        parts.any? { |part| query.include? part }
      }
      return partial_matched unless partial_matched.nil?

      idols.find { |idol| query.include?(idol.display_name) || idol.name.split(/[,・]/).any? { |part| query.include? part } }
    end

    def tweet_count(force_update = false, timestamp = SearchRecord.latest_update_timestamp)
      cache_until_update force_update, timestamp do
        Tweet.valid_tweets.count
      end
    end

    def user_count(force_update = false, timestamp = SearchRecord.latest_update_timestamp)
      cache_until_update force_update, timestamp do
        Tweet.valid_tweets.select(:user_id).count('distinct user_id')
      end
    end

    def tpu_count(force_update = false, timestamp = SearchRecord.latest_update_timestamp)
      u = user_count(force_update, timestamp)
      return nil if u.nil?

      t = tweet_count(force_update, timestamp)
      t.to_f / u
    end

    def tweet_until_date(date)
      timestamp = SearchRecord.latest_update_timestamp.to_date
      key = _cache_key(timestamp, "tweet_until_date/#{date.to_date}")
      Rails.cache.fetch(key) do
        _valid_tweets_until(date.end_of_day).size.to_i
      end
    end

    def tweet_of_date(date)
      timestamp = SearchRecord.latest_update_timestamp.to_date
      key = _cache_key(timestamp, "tweet_of_date/#{date.to_date}")
      Rails.cache.fetch(key) do
        tweet_until_date(date) - tweet_until_date(date.yesterday)
      end
    end

    def user_until_date(date)
      timestamp = SearchRecord.latest_update_timestamp.to_date
      key = _cache_key(timestamp, "user_until_date/#{date.to_date}")
      Rails.cache.fetch(key) do
        _valid_tweets_until(date.end_of_day).distinct.count(:user_id).to_i
      end
    end

    def user_of_date(date)
      timestamp = SearchRecord.latest_update_timestamp.to_date
      key = _cache_key(timestamp, "user_of_date/#{date.to_date}")
      Rails.cache.fetch(key) do
        user_until_date(date) - user_until_date(date.yesterday)
      end
    end

    private

    def _valid_tweets_until(date)
      range = SearchRecord.start_of_campaign...date
      Tweet.valid_tweets.where(created_at: range)
    end

    def _cache_key(timestamp, name)
      "Idol.self/#{timestamp}/#{name}"
    end

    def cache_until_update(force_update, timestamp, &proc)
      key = _cache_key timestamp, caller_locations(1,1)[0].label
      if force_update
        value = yield proc
        Rails.cache.write(key, value)
        value
      else
        Rails.cache.fetch(key, &proc)
      end
    end

  end

  def tweet_count(force_update = false, timestamp = SearchRecord.latest_update_timestamp)
    cache_until_update force_update, timestamp do
      Idol.tweet_count_map[id].to_i
    end
  end

  def user_count(force_update = false, timestamp = SearchRecord.latest_update_timestamp)
    cache_until_update force_update, timestamp do
      Idol.user_count_map[id].to_i
    end
  end

  def tpu_count(force_update = false, timestamp = SearchRecord.latest_update_timestamp)
    u = user_count(force_update, timestamp)
    return nil if u.nil?

    t = tweet_count(force_update, timestamp)
    t.to_f / u
  end

  def display_name
    Rails.cache.fetch("#{cache_key}/display_name") do
      name.remove ','
    end
  end

  def display_yomi
    Rails.cache.fetch("#{cache_key}/display_yomi") do
      yomi.remove(',')
    end
  end

  def tweet_until_date(date)
    timestamp = SearchRecord.latest_update_timestamp.to_date
    key = _cache_key(timestamp, "tweet_until_date/#{date.to_date}")
    Rails.cache.fetch(key) do
      _valid_tweets_until(date.end_of_day).size.to_i
    end
  end

  def tweet_of_date(date)
    timestamp = SearchRecord.latest_update_timestamp.to_date
    key = _cache_key(timestamp, "tweet_of_date/#{date.to_date}")
    Rails.cache.fetch(key) do
      tweet_until_date(date) - tweet_until_date(date.yesterday)
    end
  end

  def user_until_date(date)
    timestamp = SearchRecord.latest_update_timestamp.to_date
    key = _cache_key(timestamp, "user_until_date/#{date.to_date}")
    Rails.cache.fetch(key) do
      _valid_tweets_until(date.end_of_day).distinct.count(:user_id).to_i
    end
  end

  def user_of_date(date)
    timestamp = SearchRecord.latest_update_timestamp.to_date
    key = _cache_key(timestamp, "user_of_date/#{date.to_date}")
    Rails.cache.fetch(key) do
      user_until_date(date) - user_until_date(date.yesterday)
    end
  end

  def tweet_per_user_map
    timestamp = SearchRecord.latest_update_timestamp
    key = _cache_key(timestamp, "tweet_per_user_map")
    Rails.cache.fetch(key) do
      _valid_tweets.group(:user_id).count.map{ |_, v| v }.group_by(&:itself).map {|k, v| [k, v.size] }.to_h
    end
  end

  def related_tweet_count_map
    tweeted_users = User.where(id: _valid_tweets.select(:user_id))
    map = Tweet.valid_tweets
            .where(user_id: tweeted_users.select(:id))
            .where.not(idol_id: id)
            .group(:idol_id).count.to_h

    idols = Idol.all_idols
    map.reduce({}) do |result, (id, cnt)|
      idol = idols.find{ |i| i.id == id}
      if result.include? cnt
        result[cnt].append(idol)
      else
        result[cnt] = [idol]
      end
      result
    end
  end

  def related_user_count_map
    tweeted_users = User.where(id: _valid_tweets.select(:user_id))
    map = Tweet.valid_tweets
          .where(user_id: tweeted_users.select(:id))
          .where.not(idol_id: id)
          .select(:idol_id, :user_id)
          .group(:idol_id)
          .count('distinct user_id').to_h

    idols = Idol.all_idols
    map.reduce({}) do |result, (id, cnt)|
      idol = idols.find{ |i| i.id == id}
      if result.include? cnt
        result[cnt].append(idol)
      else
        result[cnt] = [idol]
      end
      result
    end
  end

  def create_caches(timestamp = SearchRecord.latest_update_timestamp)
    tweet_count(true, timestamp)
    user_count(true, timestamp)
  end

  private

  def _valid_tweets
    tweets.valid_tweets
  end

  def _valid_tweets_until(date)
    range = SearchRecord.start_of_campaign...date
    _valid_tweets.where(created_at: range)
  end

  def _cache_key(timestamp, name)
    "#{cache_key}/#{timestamp}/#{name}"
  end

  def cache_until_update(force_update, timestamp, &proc)
    key = _cache_key timestamp, caller_locations(1,1)[0].label
    if force_update
      value = yield proc
      Rails.cache.write(key, value)
      value
    else
      Rails.cache.fetch(key, &proc)
    end
  end

end
