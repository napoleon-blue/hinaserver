class AddIndexesToTweets < ActiveRecord::Migration[5.0]
  def change
    add_index :tweets, :source_checked
    add_index :tweets, :created_at
  end
end
